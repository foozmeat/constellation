from .message import DataMessage
import random
from copy import deepcopy

"""
Represents a screen or "page" in Luminator format
"""


def _setBit(int_type, offset):
    mask = 1 << offset
    return int_type | mask


def _clearBit(int_type, offset):
    mask = ~(1 << offset)
    return int_type & mask


class Screen:
    def __init__(self, width=90, height=7):

        self.width = width
        self.height = height
        self.pix_buf = [[0] * self.height for i in range(self.width)]

        # add one to the height to pad the bytestream
        self.byteStreamSize = (self.width * (self.height + 1)) // 8
        self.bytestream = [0] * self.byteStreamSize

    def _rangeCheck(self, x, y):
        return x >= 0 and x < self.width and y >= 0 and y < self.height

    def on(self, x, y):
        if self._rangeCheck(x, y):
            self.pix_buf[x][y] = 1

    def off(self, x, y):
        if self._rangeCheck(x, y):
            self.pix_buf[x][y] = 0

    def invert(self, x, y):
        if self._rangeCheck(x, y):
            self.pix_buf[x][y] ^= 1

    def value(self, x, y):
        return self.pix_buf[x][y]

    def wrapped_value(self, x, y):
        new_x = x % self.width
        new_y = y % self.height
        return self.value(new_x, new_y)

    def all_on(self):
        self.pix_buf = [[1] * self.height for i in range(self.width)]

    def all_off(self):
        self.pix_buf = [[0] * self.height for i in range(self.width)]

    def new_buffer(self):
        return [[0] * self.height for i in range(self.width)]

    def copy_buffer(self):
        return deepcopy(self.pix_buf)

    def set_buffer(self, new_buffer):
        self.pix_buf = deepcopy(new_buffer)

    def load_image(self, image):
        if image:
            for y in range(0, self.height):
                for x in range(0, self.width):
                    self.pix_buf[x][y] = image.getpixel((x, y))

    def randomize(self):
        self.all_off()
        for row in range(0, self.height):
            for column in range(0, self.width):
                if random.random() > 0.7:
                    self.on(column, row)

    def __str__(self):
        output = "-" * self.width + "\n"
        for row in range(0, self.height):
            for column in range(0, self.width):
                v = self.pix_buf[column][row]
                if v:
                    output += "*"
                else:
                    output += " "
            output += "\n"
        output += "-" * self.width

        return output

    def reset_byteStream(self):
        self.bytestream = [int("01", 16)] + [int("10", 16)]
        self.bytestream += [int("00", 16)] + [int("00", 16)]
        self.bytestream += [0] * self.byteStreamSize

    def convert_to_data(self):

        byteCount = 4  # skip the header
        self.reset_byteStream()

        # set the pixel "bits" in the outbound data
        for column in range(self.width):
            # go through rows backwards
            for row in range(self.height - 1, -1, -1):
                if self.pix_buf[column][row]:
                    self.bytestream[byteCount] = _setBit(
                        self.bytestream[byteCount], row
                    )
                else:
                    self.bytestream[byteCount] = _clearBit(
                        self.bytestream[byteCount], row
                    )

                # if row > 0:
                # shift all but the first bit
                # self.bytestream[byteCount] <<= 1

                # print(byteCount, row, column, self.bytestream[byteCount])

            byteCount += 1

        msg_array = []
        data_offset = 0
        done = False
        while not done:
            line = ""
            while len(line) < 32:

                try:
                    b = self.bytestream.pop(0)
                    line += f"{b:X}".zfill(2)

                except IndexError:
                    done = True

                    if len(line) < 32:
                        # pad the end of the last array
                        line += "0" * (32 - len(line))

                    break
            # print(line)
            msg_array += [DataMessage(offset=data_offset, data=line)]
            data_offset += 16

        return msg_array
