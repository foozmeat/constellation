from enum import Enum

"""
Wraps up the Luminator message format
"""


class MessageEnum(Enum):
    def __str__(self):
        return str(self.value)

    def __len__(self):
        return len(self.value)


class SignState(MessageEnum):
    Unknown_00 = "00"
    Unconfigured = "0F"
    ConfigInProgress = "0D"
    ConfigReceived = "07"
    ConfigFailed = "0C"
    PixelsInProgress = "03"
    PixelsReceived = "01"
    PixelsFailed = "0B"
    PageLoaded = "10"
    PageLoadInProgress = "13"
    PageShown = "12"
    PageShowInProgress = "11"
    ReadyToReset = "08"


class MessageType(MessageEnum):
    SendData = "00"
    DataChunksSent = "01"
    Hello = "02"
    Goodbye = "02"
    QueryState = "02"
    ReportState = "04"
    RequestOperation = "03"
    AcknowledgeOperation = "05"
    PixelsComplete = "06"


class MessageData(MessageEnum):
    Hello = "FF"
    Goodbye = "55"
    QueryState = "00"
    PixelsComplete = "00"


class OperationType(MessageEnum):
    ReceiveConfig = "A1"
    ReceivePixels = "A2"
    ShowLoadedPage = "A9"
    LoadNextPAge = "AA"
    StartReset = "A6"
    FinishReset = "A7"
    Unknown_91 = "91"
    Unknown_95 = "95"


class SignMessage:
    def __init__(self, address=0, msg_type="", data=""):
        self.address: int = address
        self.msg_type: str = msg_type
        self.data: str = data
        self.raw_msg = None

        assert self.data_length <= 16

    def __str__(self):
        return ":" + self.payload + self.lrc

    def __repr__(self):

        address = f"Address: {self.address}"
        type = f"\tType: {MessageType(self.msg_type).name}"
        state = ""
        operation = ""
        data = ""

        if self.msg_type == MessageType.ReportState.value:
            state += f"\tState: {SignState(self.data).name}"

        elif self.msg_type == MessageType.AcknowledgeOperation.value:
            operation += f"\tOperation: {OperationType(self.data).name}"

        elif self.msg_type == MessageType.RequestOperation.value:
            operation += f"\tOperation: {OperationType(self.data).name}"

        elif self.msg_type == "02":
            type = f"\tType: {MessageData(self.data).name}"

        else:
            data = f"\tData: {self.data}"

        return f"{address}{type}{state}{operation}{data}"

    def parse_raw(self, raw_msg) -> None:

        self.raw_msg = raw_msg

        msg = raw_msg.decode()

        for m in msg.splitlines():

            data_length = int(m[1:3], 16)
            self.address = int(m[3:7], 16)
            self.msg_type = m[7:9]
            self.data = m[9:(9 + data_length)]
            mlrc = m[-2:]

            print(f"{m=} {data_length=} {self.address_formatted=} {self.msg_type=} {self.data=} {mlrc=}")

            if mlrc != self.lrc:
                print(f"LRC error for {self} {mlrc=} {self.lrc=}")

            # print(repr(self))

    @property
    def address_formatted(self) -> str:
        # how many two-byte ASCII "bytes" do we have
        return f"{self.address:X}".zfill(4)

    @property
    def data_length(self) -> int:
        if self.data:
            return len(self.data) // 2
        else:
            return 0

    @property
    def data_length_formatted(self) -> str:
        # how many two-byte ASCII "bytes" do we have
        return f"{self.data_length:X}".zfill(2)

    @property
    def payload(self) -> str:
        payload = self.data_length_formatted
        payload += self.address_formatted
        payload += str(self.msg_type)
        payload += str(self.data)

        return payload

    @property
    def lrc(self) -> str:
        # with help from https://github.com/hshutan/FlipDotCompendium
        # step through the data, peeling off two-characters to create a byte
        # and sum them

        s = 0
        for x in range(0, len(self.payload), 2):
            end = x + 2
            s += int(self.payload[x:end], 16)

        lrc = s & 0xFF
        lrc = (lrc ^ 0xFF) + 1
        lrc_hex = f"{lrc & 0xFF:X}".zfill(2)

        # print(f"{self.payload=} {lrc_hex=}")

        return lrc_hex


class HelloMessage(SignMessage):
    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.msg_type = MessageType.Hello
        self.data = MessageData.Hello


class GoodbyeMessage(HelloMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = MessageData.Goodbye


class RequestOperationMessage(SignMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.msg_type = MessageType.RequestOperation


class RequestReceiveConfigMessage(RequestOperationMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = OperationType.ReceiveConfig


class RequestReceivePixelsMessage(RequestOperationMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = OperationType.ReceivePixels


class DataMessage(SignMessage):
    def __init__(self, *args, offset=0, **kwargs):
        super().__init__(*args, **kwargs)
        self.offset = offset
        self.address = offset
        self.msg_type = MessageType.SendData

        assert self.data_length == 16


class DataChunksSentMessage(SignMessage):
    def __init__(self, *args, chunks=0, **kwargs):
        super().__init__(*args, **kwargs)
        self.chunks = chunks
        self.address = chunks
        self.msg_type = MessageType.DataChunksSent


class QueryStateMessage(SignMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.msg_type = MessageType.QueryState
        self.data = MessageData.QueryState


class PixelsCompleteMessage(SignMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.msg_type = MessageType.PixelsComplete
        self.data = MessageData.PixelsComplete


class RequestShowLoadedPageMessage(RequestOperationMessage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = OperationType.ShowLoadedPage
