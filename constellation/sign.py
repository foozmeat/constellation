import serial
from serial.tools.list_ports import comports
import time
from constellation.message import (
    SignMessage,
    HelloMessage,
    RequestReceiveConfigMessage,
    DataMessage,
    DataChunksSentMessage,
    QueryStateMessage,
    RequestReceivePixelsMessage,
    PixelsCompleteMessage,
    RequestShowLoadedPageMessage,
)


class Sign:
    def __init__(self, address, debug=False):

        self.address = address
        self.connection = serial.Serial(exclusive=True)
        self.connection.baudrate = 19200
        self.connection.port = [p for (p, d, h) in sorted(comports()) if "UART" in d][0]
        self.state = "00"
        self.debug = debug

        try:
            self.connection.open()
        except serial.SerialException as e:
            print(e)
            exit(1)

    def send_msg(self, msg: str) -> None:
        """
        Receives a structured Message or a string and sends it to the sign
        """

        recv_buffer = b""

        while self.connection.in_waiting > 0:
            recv_buffer += self.connection.read()  # clear the receive buffer

        msg = str(msg) + "\r\n"
        msg = msg.encode()

        send_msg = SignMessage()
        send_msg.parse_raw(msg)
        if self.debug:
            print("SEND", repr(send_msg))

        # print(f"Sending: {msg}")
        self.connection.write(msg)
        self.connection.flush()
        time.sleep(45 / 1000)

        while self.connection.in_waiting > 0:
            recv_buffer += self.connection.read()  # clear the receive buffer

        if len(recv_buffer) > 0:

            recv_msg = SignMessage()
            recv_msg.parse_raw(recv_buffer)
            if self.debug:
                print("RECV", repr(recv_msg))

    def hello(self):
        self.send_msg(HelloMessage(self.address))

    def sendConfig(self):
        self.send_msg(RequestReceiveConfigMessage(self.address))
        # sign acknowledges

        self.send_msg(
            DataMessage(offset=0, data="04200006071E1E1E0008000000000000")
        )  # send data

        self.send_msg(DataChunksSentMessage(chunks=1))
        self.queryState()

    def sendScreen(self, screen):
        self.send_msg(RequestReceivePixelsMessage(self.address))

        for data in screen.convert_to_data():
            self.send_msg(data)

        self.send_msg(DataChunksSentMessage(chunks=6))

        self.queryState()

        self.send_msg(PixelsCompleteMessage(self.address))

        self.send_msg(RequestShowLoadedPageMessage(self.address))  # request operation

        self.queryState()
        self.queryState()
        self.queryState()
        self.queryState()

    def queryState(self):
        self.send_msg(QueryStateMessage(self.address))  # query state
