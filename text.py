from constellation.screen import Screen
from constellation.sign import Sign
import time
from PIL import Image, ImageFont, ImageDraw


if __name__ == "__main__":

    sign = Sign(address=3)  # side sign
    sign.hello()
    sign.sendConfig()

    screen = Screen()
    # sign.sendScreen(screen)

    font = ImageFont.truetype("fonts/pf_tempesta_seven_bold.ttf", 8)

    for x in range(1):
        image = Image.new("1", (90, 7))
        draw = ImageDraw.Draw(image)
        draw.text((x, -4), "HELLO FRIEND", font=font, fill="#FFF")

        screen.load_image(image)
        print(screen)
        sign.sendScreen(screen)
        time.sleep(0.4)
