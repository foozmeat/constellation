from constellation.screen import Screen
from constellation.sign import Sign
import time

# From: https://www.geeksforgeeks.org/conways-game-life-python-implementation/

ON = 1
OFF = 0
vals = [ON, OFF]


def add_glider(screen, x, y):
    """
    Adds a glider centered at x,y
    """
    screen.on(x + 1, y - 1)
    screen.on(x - 1, y)
    screen.on(x + 1, y)
    screen.on(x, y + 1)
    screen.on(x + 1, y + 1)


def gol_update(screen):

    # copy grid since we require 8 neighbors
    # for calculation and we go line by line
    new_buffer = screen.copy_buffer()

    for x in range(screen.width):
        for y in range(screen.height):

            # compute 8-neighbor sum
            # using toroidal boundary conditions - x and y wrap around
            # so that the simulation takes place on a toroidal surface.
            total = (
                screen.wrapped_value(x, y - 1)
                + screen.wrapped_value(x, y + 1)
                + screen.wrapped_value(x + 1, y)
                + screen.wrapped_value(x - 1, y)
                + screen.wrapped_value(x - 1, y - 1)
                + screen.wrapped_value(x - 1, y + 1)
                + screen.wrapped_value(x + 1, y - 1)
                + screen.wrapped_value(x + 1, y + 1)
            )

            # apply Conway's rules
            # print(x, y, total)
            if screen.value(x, y) == ON:
                if (total < 2) or (total > 3):
                    new_buffer[x][y] = OFF
            else:
                if total == 3:
                    new_buffer[x][y] = ON

    # update data
    screen.set_buffer(new_buffer)


if __name__ == "__main__":

    sign = Sign(address=3)  # side sign
    sign.hello()
    sign.sendConfig()

    screen = Screen()
    sign.sendScreen(screen)
    time.sleep(1)

    screen.randomize()

    while True:
        gol_update(screen)
        print(screen)
        sign.sendScreen(screen)

        time.sleep(0.4)
        sign.queryState()
