from constellation.screen import Screen
from constellation.sign import Sign

sign = Sign(address=3)  # side sign
sign.hello()
sign.sendConfig()

screen = Screen()
screen.randomize()
print(screen)
sign.sendScreen(screen)
